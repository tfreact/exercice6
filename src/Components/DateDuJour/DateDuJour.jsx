import React, {Component, Fragment} from 'react'

class DateDuJour extends Component
{
    constructor(props)
    {
        super(props);
        //Créer une formateur permettant d'extraire le mois d'une date dans la langue choisie
        const formatter = new Intl.DateTimeFormat(this.props.lang, { month: 'long' });
        this.state={
            jour:new Date().getDate(),
            mois:formatter.format(new Date()),
            annee:new Date().getFullYear(),
            lang: this.props.lang
        };

        ///lier l'événement à la fonction  
        //Pas nécessaire pour l'appel en mode lambda ()=>
       // this.changeLang=this.changeLang.bind(this);
 
       //Obligatoire pour les fonctions sans paramètres
       this.hello = this.hello.bind(this);
       this.aurevoir = this.aurevoir.bind(this);

        //Variable "globale"
        this.Info="Change de langue";
    }

    
    changeLang(newLang, evt=null)
    {
        console.log(evt.target.dataset.lang);
        const formatter = new Intl.DateTimeFormat(newLang, { month: 'long' });
      
        this.setState(()=>
        ({
            mois:formatter.format(new Date()),
            lang:newLang
        }));
    }
    aurevoir(event)
    {
       event.preventDefault(); //js - Empeche l'exécution de l'événement par le navigateur
       document.getElementById("lol").innerHTML="AUREVOIR!!!!!"; //js
    }
    hello(event)
    {
        if(event.target.dataset.message !== undefined)
        {
            console.log(event.target.dataset.message);
        }
        else{
            console.log("Pas de message");
        }
        console.log(event);
       console.log(this.state);
    }
    render()
    {
          const {jour, mois, annee, lang} = this.state;

          return(
              <Fragment>
                  <div id="lol"></div>
                <h1>{this.Info} ({lang})</h1>
                <h2>{jour} {mois} {annee} </h2>
                <button data-message='Bonjour' onClick={this.hello}>HEllo</button>
                <button onClick={this.hello.bind(this)}>HEllo2</button>
                <button onClick={(e)=>this.hello(e)}>HEllo3</button>                
                <button onClick={()=> {
                                        console.log(this.state);
                                        }}>Hello4</button> 
                <hr/>
                <button onClick={()=>this.changeLang('fr')}>French</button>
                <button id="btnen" data-lang='en' onClick={(e)=>this.changeLang('en',e)}>English</button><br/>
                <button onClick={(e)=>this.changeLang('es',e)}>Spanish</button><br/>
                <button onClick={()=>this.changeLang('pl')}>Polish</button><br/>
                <button onClick={()=>this.changeLang('sw')}>Swahili</button><br/>
                <button onClick={()=>this.changeLang('ar')}>Arabic</button><br/>
                <button onClick={()=>this.changeLang('ar-DZ')}>Algeria</button><br/>
                <button onClick={()=>this.changeLang('el')}>Greek</button><br/>

                <a href="http://www.leforem.be" onClick={this.aurevoir}>Je suis 100% prêt pour un job!</a>
            </Fragment>
          );

    }
}

export default DateDuJour;
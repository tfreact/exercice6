import logo from './logo.svg';
import './App.css';
import DateDuJour from './Components/DateDuJour/DateDuJour';

function App() {
  return (
    <div className="App">
       <h2>La date du jour</h2>
       <DateDuJour lang='fr'/> 
       <DateDuJour lang='zh'/> 
    </div>
  );
}

export default App;
